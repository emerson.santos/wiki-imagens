# Repositórios de imagens para os projetos de WIKIS e manuais

Neste tutorial irei demonstrar a estrutura do repositório de imagens da ASBPM.

* Referências:
    * https://www.asbpm.com.br/

Informações
----------

- Este é o repositório de imagens para os projetos da ASBPM para Wikis e manuais.
- Favor **ORGANIZAR** as imagens conforme a estrutura de pastas definida neste repositório.

Créditos
----------

Tutorial criado para a equipe da ASBPM para ser utilizado na publicação de projetos.

- Criado por: [Ely Gonçalves](https://www.linkedin.com/in/elygoncalves/)
- Criado em: 06/06/2019 15:30
- Atualizado em: 06/06/2019 17:00
