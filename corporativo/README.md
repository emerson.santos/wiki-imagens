# Repositórios de imagens para os projetos de WIKIS e manuais

Login
----------
![Imagem](/corporativo/logo-asbpm.png)

Créditos
----------

Tutorial criado para a equipe da ASBPM para ser utilizado na publicação de projetos.

- Criado por: [Ely Gonçalves](https://www.linkedin.com/in/elygoncalves/)
- Criado em: 07/06/2019 17:33
- Atualizado em: 07/06/2019 17:33
