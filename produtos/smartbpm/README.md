# Repositórios de imagens para os projetos de WIKIS e manuais

Login
----------
![Imagem](/produtos/smartbpm/login.png)

Trocar senha
----------
![Imagem](/produtos/smartbpm/trocar-senha-usuarios.png)

Menu usuários
----------
![Imagem](/produtos/smartbpm/menu-usuarios.png)

Criar usuários
----------
![Imagem](/produtos/smartbpm/criar-usuarios.png)

Listar usuários
----------
![Imagem](/produtos/smartbpm/listar-usuarios.png)

Listar usuários e grupos
----------
![Imagem](/produtos/smartbpm/listar-usuarios-grupos.png)

Créditos
----------

Tutorial criado para a equipe da ASBPM para ser utilizado na publicação de projetos.

- Criado por: [Ely Gonçalves](https://www.linkedin.com/in/elygoncalves/)
- Criado em: 07/06/2019 17:25
- Atualizado em: 07/06/2019 17:31
